from flask import Flask, render_template
import pandas as pd
import plotly
import plotly.express as px
import json
import geojson
import logging
import plotly.graph_objects as go
from geopy.geocoders import Nominatim
from utils.convert_methods import convert_sec_to_min
from utils.ip_address_class import PublicIpAddress

from shapely.geometry import Point, shape
from shapely.geometry.polygon import Polygon

logging.basicConfig(filename='logs/app_logs.log', level=logging.INFO)

app = Flask(__name__)

DEFAULT_HEIGHT = 500
CENTERED_TITLE = 0.5
FONT_SIZE = 12


@app.route('/')
def main_page():
    """
    Metodo que se encargara de recoger los diferentes datos que se van a mostrar en la pagina principal y
    renderizar dicha pagina con dichos datos.
    :return:
    """

    properties = []
    ip_addr = PublicIpAddress().obtain_public_ip_address()
    with open('static/files/dataset_motos.geojson') as f:
        motos_info = geojson.load(f)

    # Extraer y guardas las 'properties' del fichero geojson
    for elem in motos_info['features']:
        properties.append(elem['properties'])

    df = pd.DataFrame(properties)
    df.to_pickle('static/files/moto_info.pkl')

    # Incluir los graficos a mostrar en la pagina principal: el mapa de calor, la tabla y el histograma
    heatmap = mapa_calor(motos_info)
    tableinfo = create_info_table()
    histogram = histogram_data()
    message = get_nearest_area(ip_addr)
    if 'id' in message.keys() and message['id']:
        user_area = get_map_by_uniq_id(message)
        return render_template('main_page.html', heatmap=heatmap, tableinfo=tableinfo, histogramgraph=histogram, usermap=user_area, data=message)
    else:
        user_area = {}
        return render_template('main_page.html', heatmap=heatmap, tableinfo=tableinfo, histogramgraph=histogram, usermap=user_area, data=message)


def get_map_by_uniq_id(message):
    """
    Metodo que se encarga de crear un mapa que muestre la zona a la que pertenece el usuario.
    :param message: Objeto message que se utilizara para comprobar la zona del usuario y mostrar informacion
    :return fig(PlotlyJSONEncoder): Mapa calor con la zona del usuario.
    """

    with open('static/files/dataset_motos.geojson') as f:
        motos_info = geojson.load(f)

    for elem in motos_info['features']:
        if elem['properties']['polygon'] == message['id']:
            values = elem['properties']
            values.update(message)
            to_use_df = pd.DataFrame([values])
            fig = px.choropleth_mapbox(to_use_df, geojson=motos_info, locations='polygon', color='count',
                                       color_continuous_scale="Viridis",
                                       hover_name="polygon",
                                       hover_data=["count", "ip", "out"],
                                       range_color=(0, elem['properties']['count']),
                                       mapbox_style="carto-positron",
                                       zoom=12, center={"lat": float(PublicIpAddress().obtain_coordinates_from_ip(message['ip'])[0]),
                                                         "lon":  float(PublicIpAddress().obtain_coordinates_from_ip(message['ip'])[1])},
                                       opacity=0.7,
                                       height=DEFAULT_HEIGHT,
                                       )
            fig.update_layout(margin={"r": 0, "t": 0, "l": 0, "b": 0})
            fig.update_geos(fitbounds="locations", visible=False)

            return json.dumps(fig, cls=plotly.utils.PlotlyJSONEncoder)


def mapa_calor(motos_info):
    """
    Metodo que se encarga, a partir de los datos proporcionados, de realizar un mapa de calor.
    :param motos_info(Geojson): Geojason cargado
    :return fig(PlotlyJSONEncoder): Objeto de tipo PlotlyJSONEncoder que contiene el mapa de calor
    """
    try:
        df = pd.read_pickle('static/files/moto_info.pkl')

        # Obtener coordenadas de la ciudad de Madrid
        city_coordinates_geolocator = Nominatim(user_agent="cityLocator")
        city_location = city_coordinates_geolocator.geocode("Madrid")
        lat, lon = city_location.latitude, city_location.longitude

        # Obtener para el grafico el num maximo de motos de una grid
        max_moto_count = df['count'].max()

        fig = px.choropleth_mapbox(df, geojson=motos_info, locations='polygon', color='count',
                                   color_continuous_scale="Viridis",
                                   hover_name="polygon",
                                   hover_data=["perc", "time_stopped", "vehicle_id"],
                                   range_color=(0, max_moto_count),
                                   mapbox_style="carto-positron",
                                   zoom=9.9, center={"lat": lat, "lon": lon},
                                   opacity=0.7,
                                   height=DEFAULT_HEIGHT,
                                   )
        fig.update_layout(margin={"r": 0, "t": 0, "l": 0, "b": 0})
        fig.update_geos(fitbounds="locations", visible=False)

        return json.dumps(fig, cls=plotly.utils.PlotlyJSONEncoder)
    except (ValueError, UnicodeError, Exception) as err:
        logging.error(f"Error {err} al intentar crear el mapa de calor")


def create_info_table():
    """
    Metodo que se encarga en leer un objeto de tipo pickle local y
    crea una tabla a partir de las informaciones sacadas.
    :return info_table(PlotlyJSONEncoder): Objeto de tipo PlotlyJSONEncoder que contiene la tabla con todos los datos asociados
    """

    try:
        table_fields = ['Cantidad total de motos disponibles', 'Máximo tiempo parada de un grid', 'Grid id con máximo tiempo de parada (campo polygon)',
                        'Media total de tiempo de parada de todos los grids']
        table_values = []

        df = pd.read_pickle('static/files/moto_info.pkl')

        table_values.append(df['count'].sum())
        # Maximo tiempo parada de un grid deberiamos coger todos los valores de 'time_stopped' * 'count' de cada poligono
        # y mirar el tiempo maximo
        df['total_time'] = df['count'] * df['time_stopped']

        # Se realiza la conversion de seconds a un formato mas legible
        table_values.append(convert_sec_to_min(df['total_time'].max()))

        id_max_stoppage_time = df['total_time'].idxmax()
        # Llamar con ese id a la serie correspondiente al campo 'polygon' para sacar el id del poligono con el tiempo maximo
        table_values.append(df['polygon'][id_max_stoppage_time])

        # Se realiza la conversion de seconds a un formato mas legible
        table_values.append(convert_sec_to_min(df['total_time'].mean()))

        table_values = [table_fields, table_values]

        info_table = go.Figure(data=[go.Table(
            columnorder=[1, 2],
            columnwidth=[DEFAULT_HEIGHT, 100],
            header=dict(
                values=[['<b>Dato</b>'], ['<b>Valor</b>']],
                line_color='darkslategray',
                fill_color='royalblue',
                align=['left', 'center'],
                font=dict(color='white', size=FONT_SIZE),
                height=40
            ),
            cells=dict(
                values=table_values,
                line_color='darkslategray',
                fill=dict(color=['paleturquoise', 'white']),
                align=['left', 'center'],
                font_size=FONT_SIZE,
                height=30)
        )], layout_title_text="Tabla resumen de los datos de motos de Madrid")
        info_table.update_layout(title_x=CENTERED_TITLE)

        return json.dumps(info_table, cls=plotly.utils.PlotlyJSONEncoder)
    except (ValueError, UnicodeError, Exception) as err:
        logging.error(f"Error {err} al intentar crear la tabla con la info")


def histogram_data():
    """
    Metodo que se encarga de parsear los datos sacados a partir del geojson y crear el
    histograma, donde el eje X representa número de motos y en el eje y, cantidad de grids con ese número de motos.
    :return fig(PlotlyJSONEncoder): Objeto de tipo PlotlyJSONEncoder que contiene el histograma
    """
    try:
        data_representation = {}

        with open('static/files/dataset_motos.geojson') as f:
            motos_info = geojson.load(f)

        for elem in motos_info['features']:
            if elem['properties']['count'] in data_representation.keys():
                data_representation[elem['properties']['count']] += 1
            else:
                data_representation.update({elem['properties']['count']: 1})

        df = pd.DataFrame(data_representation.items(), columns=['Cantidad motos', 'Cantidad grids'])
        num_categorical_data = (df['Cantidad motos'].max()).item()
        fig = px.histogram(df, x="Cantidad motos", y="Cantidad grids", title="Histograma cantidad_motos-grid", nbins=num_categorical_data)
        fig.update_layout(title_x=CENTERED_TITLE, bargap=0.1, height=DEFAULT_HEIGHT)

        return json.dumps(fig, cls=plotly.utils.PlotlyJSONEncoder)
    except (ValueError, UnicodeError, Exception) as err:
        logging.error(f"Error {err} al intentar crear el histograma")


def get_nearest_area(ip: str, postal_code=None):
    """
    Metodo que dada una direccion ip o codigo postal va a mirar o encontrar el area disponible mas cercano y va a mostrar sus datos.
    :param ip: Direccion ip que se va a utilizar para determinar el poligono/area mas cercana
    :param postal_code: Codigo postal sobre el cual se va a mirar para determinar el poligono/area mas cercana
    :return message(Dict): Diccionario con la informacion de respuesta que comprueba si dada una ip hay motos disponibles en el area en la que pertenece
    """

    # Obtiene las coordenadas del usuario
    coords = PublicIpAddress().obtain_coordinates_from_ip(ip)
    lat, lon = float(coords[0]), float(coords[1])
    check_coords = Point(lon, lat)

    message = {}
    with open('static/files/dataset_motos.geojson') as f:
        motos_info = geojson.load(f)

    for elem in motos_info['features']:
        polygon = shape(elem['geometry'])
        if polygon.contains(check_coords):
            message.update({'id': elem['properties']['polygon'], 'out': f"Hay {elem['properties']['count']} moto(s) disponible(s) en su área.", 'ip':
                            ip})
    if len(message) == 0:
        message.update({'id': None, 'out': f"No hay motos disponible(s). en su área.", 'ip': ip})

    return message


if __name__ == '__main__':
    app.run(host="localhost", port=5002, debug=True)
    # get_nearest_area("31.4.180.148")
