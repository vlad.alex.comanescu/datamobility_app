"""
Script para obtener y manejar direcciones ip publicas.
"""

import ipaddress

from requests import get
from IPy import IP
import ipinfo
import logging

PUBLIC_IP_ADDRESS_LOOKUP_SITE = 'https://api.ipify.org'
COORDINATES_LOOKUP_SITE_TOKEN = '6aca22dece96b5'
LOCALHOST = '127.0.0.1'
logging.basicConfig(filename='logs/app_logs.log', level=logging.INFO)


class PublicIpAddress:
    """
    Clase que obtiene la direccion publica y a partir de una direccion ip publica
    obtener las coordenadas correspondientes.
    """

    @staticmethod
    def obtain_public_ip_address() -> str:
        """
        Metodo que obtiene la direccion publica ip.
        :param:
        :return public_ip (str): Direccion ip publica
        """
        try:
            public_ip = get(PUBLIC_IP_ADDRESS_LOOKUP_SITE).content.decode('utf8')
            logging.info(f"Obtained public ip: {public_ip} .")
            return public_ip
        except UnicodeDecodeError:
            logging.error(f"Failed to decode public ip address.")

    @staticmethod
    def obtain_coordinates_from_ip(public_ip: str):
        """
        Metodo que obtiene a partir de una direccion ip publica dada sus correspondientes coordenadas
        :param public_ip: Direccion ip publica
        :return coordinates: Coordenadas asociadas a la direccion ip
        """
        try:
            if public_ip == LOCALHOST:
                public_ip = PublicIpAddress().obtain_public_ip_address()

            if PublicIpAddress().check_ip_validity(public_ip):
                ip = IP(public_ip)
                if ip.iptype() == 'PUBLIC':
                    handler = ipinfo.getHandler(COORDINATES_LOOKUP_SITE_TOKEN)
                    details = handler.getDetails(public_ip)
                    coord = details.loc.split(',')
                    logging.info(f"Obtained coordinates:{coord} from address:{public_ip} .")
                    return coord
        except (ValueError, Exception) as err:
            logging.error(f"Cannot obtain coordinates from address: {public_ip} -- err:{err}.")

    @staticmethod
    def check_ip_validity(public_ip: str) -> bool:
        """
        Metodo que comprueba de que una direccion ip publica dada tiene un formato correcto.
        :param public_ip: Direccion ip publica
        :return valid: Bool True en caso de que sea valida.
        """
        try:
            valid_ip = ipaddress.ip_address(public_ip)
            logging.info(f"{valid_ip} is a correct ip address.")
            return True

        except ValueError:
            logging.error(f"Address: {public_ip} is invalid.")

        except:
            logging.error(f"Needed public_ip/ There has been an error converting {public_ip}.")


if __name__ == '__main__':
    pass
