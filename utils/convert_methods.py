"""
Metodos que se utilizan para convertir fechas / distintos tipos de datos
"""
import datetime


def convert_sec_to_min(seconds):
    """
    Metodo que se encarga de recibir un numero de segundos y convertirlo a minutos
    :return min: Numero de minutos que corresponde a los segundos dados
    """
    return str(datetime.timedelta(seconds=seconds))

