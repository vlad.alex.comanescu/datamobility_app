# App Datos-motos Madrid

Esta app se encarga de mostrar ciertos datos en una pagina web simple:
- Mapa calor segun el numero de motos por las zonas de Madrid
- Tabla con algunos datos resumidos sacados de los datos de las motos
- Histograma mostrando la relaciones entre el numero de motos y el numero
de grids con ese numero de motos

## Instrucciones para crear y correr la app

1. Se importa el proyecto: ```git clone https://gitlab.com/vlad.alex.comanescu/datamobility_app.git```

2. Se crea un venv usando el siguiente comando en la raiz del proyecto:
```python3 -m venv venv```
3. Entramos en el entorno virtual: ```source venv/bin/activate```
4. Instalamos los paquetes: ```pip3 install -r requirements.txt```
5. Instanciamos los directorios 'logs' y 'static' en la raiz.
6. En 'static' tenemos que crear la carpeta 'files' y añadir el fichero 'dataset_motos.geojson'.
7. Ejecutamos el script main de la app: ```python3 main.py```

## Funcionalidades app
- Ejercicio 1: Mapa de color segun el numero de motos disponibles.
En este ejercicio se crea utilizando la libreria de plotly el mapa de calor 
a partir de un fichero geojson. Para ello, se lee/parsea el geojson y tambien
se extraen las propiedades de cada poligono. Ahi vamos a encontrar la informacion
que necesitaremos para construir el heat_map y la tabla de valores del Ejercicio 2.
Se utilizara la librerira 'geopy' para sacar las coordenadas de la ciudad de Madrid.
Creamos el heatmap, usando como parametros el 'color' fijango como valor 'count',
para determinar sobre que se va a basar el mapa de color, el numero de motos disponibles.
Fijamos con el 'hover_data=["perc", "time_stopped", "vehicle_id"]' los valores que se mostraran
cuando el usuario hace hover sobre ese poligono.
<img src="static/images/acciona1.png"/>
- Ejercicio 2: Este ejercicio trata de crear una tabla con varios datos que
vamos a computar a partir del dataframe que hemos guardado anteriormente.
Creamos un array que va a guardar los diferentes valores computados. La suma 
de todas las motos disponibiles es sumar los valores de la columna count. Para el
maximo tiempo parada de un grid, vamos a crear una nueva columna ya que, este tiempo no
se nos proporciona directamente y tenemos que calcularlo a partir del tiempo medio*cantidad(count)
de motos disponibles de ese grid. Ese sera el tiempo total de cada grid. Luego, es trivial
sacar el valor maximo de dicha columna. Obtenemos el id de la columna con ese valor maximo,
y con el id obtenemos el valor de 'polygon' correspondiente a la fila con ese id.
Por ultimo, utilizaremos el campo de 'tiempo_total' para calcular la media de todos los poligionos.
Se emplea un metodo creado, definido en 'utils' para transformar el numero de segundos
en una forma mas legible.
<img src="static/images/acciona2.png"/>

- Ejercicio 3: Se procede a la creacion de un histograma que muestre la relacion
entre el numero exacto de motos disponibles (campo 'count') y el numero/suma de 
grids unicos que tengan dicho numero de motos en su area correspondiente.
Para ello creamos una relacion utilizando un diccionario, convertimos dicho diccionario a df
y creamos el histograma que muestre la relacion entre la cantidad de motos - numero de grids 
que tengan esa cantidad. Utilizaremos, en la creacion del histograma un valor de 'nbins' 
igual a la maxima cantidad de numero de motos disponibles de todos los grids para separar 
esas relaciones en relaciones 1-1 entre el numero de motos-numero de grids.
<img src="static/images/acciona3.png"/>
- Ejercicio 4(opcional): En este ejercicio se me ha ocurrido la idea de comprobar, segun mi ip publica, 
si hay motos disponibles en mi area. Se ha desarrollado la funcionalidad para obtener la ip publica del usuario,
convertir dicha ip en coordenadas y comprobar dichas coordenadas en la funcion 'get_nearest_area' para ver cual es
el poligono al que pertenece el usuario. En caso de que esa ip/coordenadas no esten dentro de un poligono definido
en el fichero geojson, se devuelve un mensaje de que no hay motos disponibles. En caso de que se haya encontrado un
poligono, crear un mapa mostrando la zona y datos como: la ip, numero motos disponibles, id del poligono.
Este metodo puede que no represente correctamente la zona ya que la ip sacada es en funcion del ISP, y del bloque de ips
asociados a ese isp. Si se logra resolver este problema de precision, puede ser una herramienta util para que un usario,
sepa su localizacion y sepa si en su area hay motos disponibles o no.
<img src="static/images/acciona4.png"/>

Tras la definicion de las diferentes funcionalidades hay un metodo 'main_page', que se encarga de obtener los objetos tipo PlotlyJSONEncoder, que seran utilizando
en la plantilla main_page.html de 'templates' para renderizar dichos plots.


